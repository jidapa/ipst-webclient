import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {browserHistory} from 'react-router';
import 'antd/dist/antd.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Routes from './routes.js';

ReactDOM.render(<Routes history={browserHistory} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
