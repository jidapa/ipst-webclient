import React from 'react';
import{ Router, Route } from 'react-router';

import App from './views/App/App';
import Login from './views/Login/Login';
import AssetList from './views/AssetList/AssetList';
import NotFound from './views/NotFound/NotFound';

const Routes = (props) => (
    <Router {...props}>        
        <Route path="/login" component={Login} />
        <Route path="/" component={App} />
        <Route path="/asset" component={AssetList} />
        <Route path="*" component={NotFound} />
    </Router>
);

export default Routes;