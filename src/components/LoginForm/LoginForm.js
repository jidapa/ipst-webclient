import React from 'react';
import Logo from '../../assets/images/logo-ipst-white.png'
import './LoginForm.css';
import { Row, Col, Form, Icon, Input, Button, Checkbox } from 'antd';

class LoginForm extends React.Component {
    render() {
        return (
            <div className="layput-page">
                <Form className="text-style">
                    <Row type="flex">
                        <Col span={24} className="position-logo">
                            <img src={Logo} className="size-logo" alt="logoLogin" />
                        </Col>
                    </Row>
                    <Row type="flex">
                        <Col span={24} className="text-header">
                            สถาบันส่งเสริมการสอนวิทยาศาสตร์และเทคโนโลยี (สสวท.) <br />
                            The Institute for the Promotion of Teaching Science and Technology (IPST)
                    </Col>
                    </Row>
                    <Row type="flex" justify="center" style={{ marginTop: '1%' }}>
                        <Col md={7} sm={24}>
                            <span className="text-label">บัญชีผู้ใช้ หรือ อีเมล</span>
                            <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="โปรดกรอกบัญชีผู้ใช้ หรือ อีเมล" />
                        </Col>
                    </Row>
                    <Row type="flex" justify="center" style={{ marginTop: '1%' }}>
                        <Col md={7} sm={24}>
                            <span className="text-label">รหัสผ่าน</span>
                            <Input prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="โปรดกรอกรหัสผ่าน" type="password" />
                        </Col>
                    </Row>
                    <Row type="flex" justify="center" style={{ marginTop: '1%' }}>
                        <Col smd={4} sm={24}>
                            <Checkbox style={{ color: '#fff', fontWeight: '300', fontSize: '0.8em' }}>ให้ฉันอยู่ในระบบต่อไป</Checkbox>
                        </Col>
                        <Col md={3} sm={24} className="forgot-layout">
                            <a href="/" style={{ color: '#fff' }}>ลืมรหัสผ่าน?</a>
                        </Col>
                    </Row>
                    <Row type="flex" justify="center" style={{ marginTop: '2%' }}>
                        <Col span={4} style={{ textAlign: 'center' }}>
                            <Button className="btn-color" href="/">เข้าสู่ระบบ</Button>
                        </Col>
                    </Row>
                    <Row type="flex" justify="center" className="regis-layout">
                        <Col md={4} sm={24} style={{ textAlign: 'center' }}>
                            <a href="/" style={{ color: '#fff' }}><u>สมัครสมาชิก</u></a>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}

export default LoginForm;