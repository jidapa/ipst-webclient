import React from 'react';
import '../DepartmentMobile/DepartmentMobile.css';
import { Row, Col, Table, Card } from 'antd';

const columns = [
    { title: 'ชื่อครุภัณฑ์', dataIndex: 'asset_name', key: 'asset_name' },
];

class DepartmentMobile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dept_name: "",
        }
    }

    render() {
        var count = 0;
        const data = [];
        const expandedRowRender = () => {
            const columns = [
                { title: 'asset_id', dataIndex: 'asset_id', key: 'asset_id' },
                { title: 'asset_status', dataIndex: 'asset_status', key: 'asset_status' },
                { title: 'warranty', dataIndex: 'warranty', key: 'warranty' },
                { title: 'date', dataIndex: 'date', key: 'date' },
                { title: 'history_repair', dataIndex: 'history_repair', key: 'history_repair' },
                { title: 'update_by', dataIndex: 'update_by', key: 'update_by' },
                { title: 'update_date', dataIndex: 'update_date', key: 'update_date' },
            ];

            const cardDetail = (
                <Card style={{ width: 300 }}>
                    <Row>
                        <Col span={12}>
                            หมายเลขครุภัณฑ์
                        </Col>
                        <Col span={12}>
                            {data[count].asset_id}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            สถานะครุภัณฑ์
                        </Col>
                        <Col span={12} style={{ color: '#23A670', fontWeight: '500' }}>
                            {data[count].asset_status}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            สถานที่ตั้งของครุภัณฑ์
                        </Col>
                        <Col span={12}>
                            {this.props.dept_name}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            การรับประกัน
                        </Col>
                        <Col span={12}>
                            {data[count].warranty}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            วันที่รับ
                        </Col>
                        <Col span={12}>
                            {data[count].date}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            ประวัติการซ่อม
                        </Col>
                        <Col span={12}>
                            {data[count].history_repair}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            มีการแก้ไขโดย
                        </Col>
                        <Col span={12}>
                            {data[count].update_by}
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12}>
                            แก้ไขล่าสุด
                        </Col>
                        <Col span={12}>
                            {data[count].update_date}
                        </Col>
                    </Row>
                </Card>
            );
            count++;
            return cardDetail;
        };

        for (let i = 1; i < 10; ++i) {
            data.push({
                "key": i,
                "asset_id": "47-4261-027-09",
                "asset_name": "เครื่องไมโครคอมพิวเตอร์ ยี่ห้อ IBM เครื่องที่" + i,
                "asset_status": "ปกติ",
                "warranty": "2 ปี",
                "date": "28/9/2554",
                "history_repair": "-",
                "update_by": "จิดาภา ปัตตานัง",
                "update_date": "10/10/2554"
            });
        }
        return (
            <div style={{ marginTop: '5%', marginBottom: '20%' }}>
                <Row>
                    <Col span={24} className="text-topic-dept">
                        แสดงข้อมูลครุภัณฑ์ทั้งหมดของ{this.props.dept_name}
                    </Col>
                </Row>
                <Row style={{ padding: '3%' }}>
                    <Col span={24} className="text-topic-dept">
                        <Table
                            className="components-table-demo-nested"
                            columns={columns}
                            expandedRowRender={expandedRowRender}
                            dataSource={data}
                        />
                    </Col>
                </Row>
            </div>
        )
    }
}

export default DepartmentMobile;