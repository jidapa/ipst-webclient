import React from 'react';
import { Layout } from 'antd';
import '../../components/Footer/Footer.css'

const { Footer } = Layout;

class FooterLayout extends React.Component {
    render() {
        return (
            <Layout className="layout">
                <Footer style={{ textAlign: 'center'}} className="bg-footer">© 2019 - ITPS @ G-Able</Footer>
            </Layout>
        )
    }
}

export default FooterLayout;