import React from 'react';
import '../AssetMobile/AssetMobile.css';
import { Row, Col, Card, Button, Icon } from 'antd';
import Chair from '../../assets/images/probook.png';
import EditIcon from '../../assets/images/edit-status.svg'

class AssetMobile extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            edit: true
        }
    }

    render() {
        return (
            <div style={{ marginTop: '5%' }}>
                <Row>
                    <Col style={{ textAlign: 'center', fontSize: '1.2em', fontWeight: '600' }}>
                        ข้อมูลครุภัณฑ์
                    </Col>
                </Row>
                <center>
                    <Row style={{ marginTop: '5%' }}>
                        <Col>
                            <Card style={{ width: 300 }}>
                                <p style={{ textAlign: 'center', fontSize: '1.2em', fontWeight: '600' }}>HP ProBook 440 G5 Notebook PC</p>
                                <Row>
                                    <Col span={24}>
                                        <center><img src={Chair} style={{ width: '100px' }} /></center>
                                    </Col>
                                </Row>
                                <div style={{ textAlign: 'start' }}>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            ข้อมูลครุภัณฑ์
                                        </Col>
                                        <Col span={12}>
                                            47-4261-027-00
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            สถานะครุภัณฑ์ 
                                        </Col>
                                        <Col span={12}>
                                            <Button className="btn-green">ปกติ</Button> <img src={EditIcon} style={{width: '15px'}} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            ประวัติการซ่อม
                                        </Col>
                                        <Col span={12}>
                                            ไม่มีประวัติการซ่อม
                                </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            หน่วยงานเจ้าของ
                                        </Col>
                                        <Col span={12}>
                                            สาขาประเมินมาตรฐาน
                                </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            S/N NO.
                                        </Col>
                                        <Col span={12}>
                                            KHWG66C
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            สถานที่ตั้งของครุภัณฑ์
                                        </Col>
                                        <Col span={12}>
                                            ห้องประชุมเทคโนโลยีสารสนเทศ
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            การรับประกัน
                                        </Col>
                                        <Col span={12}>
                                            1 ปี
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            วันที่รับ
                                </Col>
                                        <Col span={12}>
                                            28/9/2556
                                </Col>
                                    </Row>
                                    <Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            มีการแก้ไขโดย
                                </Col>
                                        <Col span={12}>
                                            จิดาภา ปัตตานัง
                                </Col>
                                    </Row><Row>
                                        <Col span={12} style={{fontWeight: '600'}}>
                                            แก้ไขล่าสุด
                                </Col>
                                        <Col span={12}>
                                            10/10/2556
                                </Col>
                                    </Row>
                                </div>
                            </Card>
                        </Col>
                    </Row>
                </center>
            </div>
        )
    }
}

export default AssetMobile;