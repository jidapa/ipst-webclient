import React from 'react';
import './Navbars.css';
import LogoFull from '../../assets/images/logofull-ipst-white.png';
import Logo from '../../assets/images/logo-ipst-white.png';
import User from '../../assets/images/user.png';
import Logout from '../../assets/images/logout.svg';
import { Layout, Menu, Row, Col } from 'antd';
import {Link} from 'react-router'

const { Header } = Layout;

class Navbars extends React.Component {
    render() {
        return (
            <div>
                <Layout>
                    <Header>
                        <Row type="flex">
                            <Col span={18} xs={20} order={1}>
                                <div className="logo">
                                    <Link to="/" ><img src={LogoFull} alt="logo" className="logo-nav-full" /></Link>
                                    <Link to="/" ><img src={Logo} alt="logo" className="logo-nav" /></Link>
                                </div>
                            </Col>
                            <Col span={6} xs={4} order={2} className="top-menu">
                                <Menu
                                    theme="dark"
                                    mode="horizontal"
                                    style={{ lineHeight: '64px' }}
                                    className="position-menu"
                                >
                                    <Menu.Item key="1">
                                        <img src={User} className="img-user-size" alt="img-user-size" />
                                        สวัสดี, คุณ นิรนาม
                                    </Menu.Item>
                                    <Menu.Item key="2">
                                    <Link to="/" ><img src={Logout} className="img-logout-size" alt="img-logout-size" /></Link>
                                    </Menu.Item>
                                </Menu>
                            </Col>
                        </Row>
                    </Header>
                </Layout>
            </div>
        );
    }
}

export default Navbars;