import React from 'react';
import '../PreviewQrCode/PreviewQrCode.css';
import { Row, Col } from 'antd';

function PreviewQrCode(props) {
    return (
        <Col span={6}>
            {props.qrCode}
        </Col>
    )
}

export default PreviewQrCode;