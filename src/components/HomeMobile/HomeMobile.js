import React from 'react';
import '../HomeMobile/HomeMobile.css';
import { Row, Col, Button } from 'antd';
import ExampleQrcode from '../../assets/images/qrcode.png';
import QrReader from 'react-qr-reader'

class HomeMobile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showResults: false,
            delay: 100,
            result: 'No result',
        }
        this.handleScan = this.handleScan.bind(this)
    }

    CallingScanner() {
        this.setState({
            showResults: true
        });
    }

    handleScan(data) {
        this.setState({
            result: data,
        })
        if (this.state.result != null) {
            window.location = this.state.result;
        }
    }
    handleError(err) {
        console.error(err)
    }

    render() {
        return (
            <div>
                {this.state.showResults === false ?

                    <div className="menu-scann">
                        <div style={{ marginTop: '5%' }}></div>
                        <Row justify="center">
                            <Col span={24} className="text-topic-mobile">
                                ตรวจสอบครุภัณฑ์
                            </Col>
                            <Col span={24} style={{ height: '250px', textAlign: 'center' }}>
                                <img src={ExampleQrcode} className="size-qrcode" />
                            </Col>
                            <Col span={24} className="text-topic" style={{ marginTop: '5%', height: '220px', textAlign: 'center' }}>
                                <Button size="large" className="btn-blue-mobile" onClick={() => this.CallingScanner()}>สแกน QR Code</Button>
                            </Col>
                        </Row>
                    </div>
                    :
                    <div className="scanner" >
                        <Row>
                            <Col span={24} className="text-qrscanner">
                                สแกน QR Code ภายในพื้นที่ที่ระบุ
                            </Col>
                        </Row>
                        <Row>
                            <Col className="position-scanner">
                                <center>
                                    <QrReader
                                        delay={300}
                                        onError={this.handleError}
                                        onScan={this.handleScan}
                                        style={{ width: '100%' }}
                                    />
                                </center>
                            </Col>
                        </Row>
                    </div>
                }

            </div>
        )
    }
}

export default HomeMobile;