import React from 'react'
import './Login.css';
import LoginForm from '../../components/LoginForm/LoginForm';
import BgImage from '../../assets/images/ipst-background-bl.jpg';

class Login extends React.Component {
    render() {
        return (
             <div className="top-page">
                 <img src={BgImage} alt="bg" className="bg"/>
                 <LoginForm />
             </div>
        );
    }
}

export default Login;