import React from 'react'
import './AssetList.css';
import Navbars from '../../components/Navbars/Navbars'
import Footer from '../../components/Footer/Footer'
import { Row, Col, Button, Table, Card } from 'antd';
import ComputerImg from '../../assets/images/t1.png';
import AssetMobile from '../../components/AssetMobile/AssetMobile';

const columns = [
    {
        title: 'ชื่อครุภัณฑ์',
        dataIndex: 'name',
        defaultSortOrder: 'ascend',
        sorter: (a, b) => a.name.length - b.name.length,
        sortDirections: ['descend', 'ascend'],
    }
];

const data = [];
for (let i = 1; i < 46; i++) {
    data.push({
        key: i,
        name: `ชุดอุปการณ์คอมพิวเตอร์เครื่องที่ ${i}`,
    });
}

function onChange(pagination, filters, sorter) {
    console.log('params', pagination, filters, sorter);
}

const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};

class AssetList extends React.Component {
    render() {
        return (
            <div>
                <Navbars />
                <div className="layout-page">
                    <div style={{ padding: '0 10rem', marginTop: '3%' }}>
                        <Row type="flex" justify="space-around">
                            <Col span={12}>
                                <Button href="/" className="btn-gray" icon="arrow-left">
                                    ย้อนกลับ
                            </Button>
                                <span className="text-dept-name">ข้อมูลครุภัทฑ์ “สาขาวิทยาการคอมพิวเตอร์”</span>
                            </Col>
                            <Col span={10} style={{ textAlign: 'end', fontWeight: '500', fontSize: '0.9em' }}>
                                เลือกทั้งหมด <span className="text-select">0</span> รายการ
                        <Button type="primary" icon="printer" className="btn-blue">
                                    พิมพ์ QR Code
                        </Button>
                            </Col>
                        </Row>
                        <Row style={{ marginTop: '1em' }} type="flex" justify="space-around">
                            <Col span={10}>
                                <Table columns={columns} rowSelection={rowSelection} dataSource={data} onChange={onChange} />
                            </Col>
                            <Col span={12}>
                                <Card className="card-detail" title="รายละเอียดครุภัณฑ์" style={{ width: '100%', height: '425px' }}>
                                    <img src={ComputerImg} alt="computer" style={{ width: '39y0px', marginTop: '-10px' }} />
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>
                <div className="asset-mobile">
                    <AssetMobile />
                </div>
                <div style={{ marginTop: '2em' }}></div>
                <Footer />
            </div>
        )
    }
}

export default AssetList;