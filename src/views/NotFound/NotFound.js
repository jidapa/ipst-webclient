import React from 'react'
import './NotFound.css';
import ResultDemo from '../../components/NotFound/NotFound'

class NotFound extends React.Component{
    render(){
        return (
            <div>
                <h1><ResultDemo/></h1>
            </div>
        )
    }
}

export default NotFound;