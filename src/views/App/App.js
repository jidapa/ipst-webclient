import React from 'react';
import './App.css';
import Navbars from '../../components/Navbars/Navbars';
import Footer from '../../components/Footer/Footer';
import { Card, Row, Col, Button, Checkbox, Skeleton, message } from 'antd';
import pathommawai from '../../assets/department/1-pathommawai.jpg';
import math from '../../assets/department/2-math.jpg';
import sciencePathom from '../../assets/department/3-science-pathom.jpg';
import scienceMatthayom from '../../assets/department/4-science-matthayom.jpg';
import physics from '../../assets/department/5-physics.jpg';
import chemical from '../../assets/department/6-chemical.jpg';
import biology from '../../assets/department/7-biology.jpg';
import astronomy from '../../assets/department/8-astronomy.jpg';
import designAndTech from '../../assets/department/9-design-and-tech.jpg';
import technology from '../../assets/department/10-technology.jpg';
import research from '../../assets/department/11-research.jpg';
import assessment from '../../assets/department/12-assessment.jpg';
import QRCode from 'qrcode.react';
import HomeMobile from '../../components/HomeMobile/HomeMobile';
import DepartmentMobile from '../../components/DepartmentMobile/DepartmentMobile'

document.body.style.backgroundColor = "#DFE2E6";
const { Meta } = Card;
class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      count: 0,
      selectItems: [],
      qrCode: [],
      qrForPrint: "",
      dataQrCode: [],
      showResults: false,
      dept_name: "",
      items: [
        {
          "dept_id": "90-4261-027-00",
          "dept": "สาขาปฐมวัย",
          "isChecked": false,
          "img": pathommawai
        },
        {
          "dept_id": "90-4261-027-01",
          "dept": "สาขาคณิตศาสตร์(ประถมศึกษา มัธยมศึกษา)",
          "isChecked": false,
          "img": math
        },
        {
          "dept_id": "90-4261-027-02",
          "dept": "สาขาวิทยาศาสตร์ประถมศึกษา",
          "isChecked": false,
          "img": sciencePathom
        },
        {
          "dept_id": "90-4261-027-03",
          "dept": "สาขาวิทยาศาสตร์มัธยม(ตอนต้น)",
          "isChecked": false,
          "img": scienceMatthayom
        },
        {
          "dept_id": "90-4261-027-04",
          "dept": "สาขาฟิสิกส์",
          "isChecked": false,
          "img": physics
        },
        {
          "dept_id": "90-4261-027-05",
          "dept": "สาขาเคมี",
          "isChecked": false,
          "img": chemical
        },
        {
          "dept_id": "90-4261-027-06",
          "dept": "สาขาชีววิทยา",
          "isChecked": false,
          "img": biology
        },
        {
          "dept_id": "90-4261-027-07",
          "dept": "สาขาโลก ดาราศาสตร์และอวกาศ",
          "isChecked": false,
          "img": astronomy
        },
        {
          "dept_id": "90-4261-027-08",
          "dept": "สาขาการออกแบบและเทคโนโลยี",
          "isChecked": false,
          "img": designAndTech
        },
        {
          "dept_id": "90-4261-027-09",
          "dept": "สาขาเทคโนโลยี (คอมพิวเตอร์)",
          "isChecked": false,
          "img": technology
        },
        {
          "dept_id": "90-4261-027-20",
          "dept": "สาขาวิจัย",
          "isChecked": false,
          "img": research
        },
        {
          "dept_id": "90-4261-027-21",
          "dept": "สาขาประเมินมาตรฐาน",
          "isChecked": false,
          "img": assessment
        }
      ]
    }
  }

  componentDidMount() {
    this.GetIdFromURL();
  }

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  }

  togglePageLoad() {
    this.setState({ loading: false })
  }
  componentWillMount() {
    setTimeout(() => {
      this.togglePageLoad();
    }, 900);
  }
  onChange = (event, value) => {
    if (this.state.selectItems.length === 0) {
      this.setState({
        selectItems: [...this.state.selectItems, value],
      });
    }
    else {
      if (this.state.selectItems.includes(value) === true) {
        var index = this.state.selectItems.indexOf(value)
        this.state.selectItems.splice(index, 1);
      }
      else {
        this.setState({
          selectItems: [...this.state.selectItems, value],
        });
      }
    }

    let items = this.state.items;
    let checkStatus = false;
    items.forEach(item => {
      if (item.value === event.target.value) {
        item.isChecked = event.target.checked;
        checkStatus = item.isChecked;
      }
    })
    if (checkStatus === true) {
      this.setState(({ count }) => ({
        count: count + 1
      }));
    }
    else {
      this.setState(({ count }) => ({
        count: count - 1
      }));
    }
    this.setState({ items: items })
  }

  GenerateQrCode(event, countSelect) {
    if (countSelect === 0) {
      message.config({
        top: 90,
        duration: 2,
        maxCount: 3,
      });
      message.error('คำเตือน: กรุณาเลือกหน่วยงานที่ต้องการพิมพ์ QR Code');
    }
    else {
      let data = "";
      let dataList = []
      this.state.qrCode = []
      this.state.selectItems.forEach(element => {
        data = "https://35.186.146.15?id=" + element.dept_id + "&dept=" + element.dept;
        dataList.push(data);
        const qrWithLabel = (
          <div>
            <Card style={{ width: '97px', height: '97px', textAlign: 'center' }} className="card-qr">
              <QRCode value={data} style={{ width: '80px', height: '80px' }} />
              <Meta title={element.dept_id} className="text-code-qr" />
            </Card>
          </div>
        )
        this.state.qrCode.push(qrWithLabel);
        this.setState({
          qrCode: this.state.qrCode
        })
      })
      const listQrcode = this.state.qrCode.map((itemQrCode, index) =>
        <Col span={6} key={index}>{itemQrCode}</Col>
      );
      const PreviewQrCode = (
        <div style={{ width: '386px', height: '100%', backgroundColor: '#fff' }} >
          <Row id="qrcode">
            {listQrcode}
          </Row>
        </div>
      )
      this.setState({
        qrForPrint: PreviewQrCode
      })

      setTimeout(() => {
        this.PrintQr();
      }, 100);
    }
  }

  PrintQr() {
    window.print()
  }

  GetIdFromURL() {
    var url = window.location.pathname;
    var id = this.props.location.query.id;
    var dept = this.props.location.query.dept;
    if (id != null) {
      this.setState({
        showResults: true,
        dept_name: dept
      });
    }
  }

  render() {
    const { loading } = this.state;
    const countData = Object.keys(this.state.items).length;
    const listItems = this.state.items.map((item) =>
      <Col sm={6} md={4} offset={1} style={{ marginRight: '-2%', marginBottom: '1.5%' }} key={item.dept_id}>
        <Card style={{ width: '100%' }} className="shadow">
          <Skeleton loading={loading} active>
            <div style={{ textAlign: 'center' }}>
              <img src={item.img} style={{ width: '100%' }} alt="defaultImg" />
              <div className="text-topic">
                {item.dept}
              </div>
              <Checkbox onChange={(event, value) => this.onChange(event, item)} >
                <Button href="/asset" className="color-btn-view" style={{ fontSize: '0.7em' }}>
                  ดูข้อมูลครุภัณฑ์
                  </Button>
              </Checkbox>
            </div>
          </Skeleton>
        </Card>
      </Col>
    );
    return (
      <div>
        <div className="qrcode-print">
          {this.state.qrForPrint}
        </div>
        <div className="App">
          <Navbars />
          {this.state.showResults === false ?
            <div>
              <div className="mobile-size">
                <HomeMobile />
              </div>
              <div className="body-layout">
                <div style={{ marginTop: '2em' }}></div>
                <div className="layout-page">
                  <Row className="text-style">
                    <Col span={12}>
                      แสดงหน่วยงานทั้งหมด (<span className="text-select">{countData}</span>)
                    </Col>
                    <Col span={12} style={{ textAlign: 'end' }} >
                      เลือกทั้งหมด <span className="text-select">{this.state.count}</span> รายการ
                    <Button type="primary" icon="printer" className="btn-blue" onClick={(event, countSelect) => this.GenerateQrCode(event, this.state.count)}>
                        พิมพ์ QR Code
                    </Button>
                    </Col>
                  </Row>
                  <Card style={{ marginTop: '1%', paddingLeft: '0 0 0 5%' }}>
                    <Row type="flex" justify="start">
                      {listItems}
                    </Row>
                  </Card>
                </div>
                <div style={{ marginTop: '3%' }}></div>
              </div>
            </div>
            :
            <div>
              <DepartmentMobile dept_name={this.state.dept_name} />
            </div>}
          <Footer className="footer" />
        </div>
      </div>
    );
  }
}

export default App;
